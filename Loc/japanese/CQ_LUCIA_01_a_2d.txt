001	（刀剣類の手入れは済んだか…）	VO_rusia.rusia_sys_0006
002	（しかし、命を預けるものだ<br>念には念を入れて足らぬと<br>いうことはない）
003	（フッ…命、か<br>俺の命そのものなどどうでもいいがな）	VO_rusia.rusia_sys_0007
004	（ひとりで生き、ひとりで死んでいく<br>これまでも、これからも…）
005	（だが、任務を遂行する前に<br>命を落とすなど、<br>傭兵として、あってはならぬことだ）
006	（仕える相手が誰であろうと、<br>戦地がどこであろうと、最善の戦果を残す…<br>それがプロフェッショナルだ）
007	ルシアの兄貴！　ここにいましたか！
008	…誰だ、お前は	VO_rusia.rusia_sys_0002
009	オレ、レシウスって言います！<br>ルシアの兄貴に傭兵として、<br>男として、心底惚れ込んでるんです！
010	…………
011	お願いです！<br>弟子にしてください！　この通り！
012	断る	VO_rusia.rusia_sys_0040
013	なっ、何でですか!?<br>そりゃ、オレはまだまだ実力不足ですけど、<br>これから死ぬ気で頑張りますから！	VO_retzius.retzius_sys_0026
014	お前今、実力不足と言ったな	VO_rusia.rusia_sys_0001
015	は、はいっ！　恥ずかしながら！
016	戦場で他人の面倒まで<br>見ているヒマも義理もない<br>実力不足なら尚更だ	VO_rusia.rusia_sys_0007
017	で、でもっ…オレは…！	VO_retzius.retzius_sys_0021
018	もういい、帰れ	VO_rusia.rusia_sys_0030
019	帰りません！　というか、<br>用事はそれだけじゃないんです！
020	…なんだ	VO_rusia.rusia_sys_0005
021	本部からの伝令です！<br>ただちに出撃せよ、と！<br>ちなみに自分と同じ部隊です！
022	オレ、マジ頑張るんで<br>よろしくお願いします、兄貴！
023	…………
024	（やれやれ…<br>小僧のお守りなんぞ、御免だ）
025	あっ！　荷物持ちますよ、兄貴！
